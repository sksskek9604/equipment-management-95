# 사내 기자재 이용관리 API

* 회사 내 기자재 사용 시 기자재 관리를 위해 필요한 기능을 담은 사원/기자재/이용내역 관리 API입니다.

---
###
### LANGUAGE
> ##### - JAVA 16
> ##### - SpringBoot 2.7.3
###

### 기능
> * 사원 관리
> > 사원 등록
> 
> > 사원 정보 가져오기
> 
> > 전체 사원 리스트
> 
> > 사원 정보 수정
>
> * 기자재 관리
> > 기자재  등록
>
> > 기자재 정보 가져오기
>
> > 전체 기자재 리스트
>
> > 기계 정보 수정
> 
> * 이용 내역 관리
> > 이용 내역 등록
> 
> > 이용 내역 가져오기
> 
> > 전체 이용 내역 리스트


---

## Swagger
> ![Swagger](./images/swagger.png)
>
## 사원 관리 API
>
> ![MemberAPI](./images/member_api.png)
## 기자재 관리 API
>
> ![EquipmentAPI](./images/equipment_api.png)
## 이용내역 API
>
> ![UsageDetailsAPI](./images/usage_details_api.png)