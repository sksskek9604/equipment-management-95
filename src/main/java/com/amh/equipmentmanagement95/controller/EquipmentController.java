package com.amh.equipmentmanagement95.controller;

import com.amh.equipmentmanagement95.model.common.CommonResult;
import com.amh.equipmentmanagement95.model.common.ListResult;
import com.amh.equipmentmanagement95.model.common.SingleResult;
import com.amh.equipmentmanagement95.model.equipment.EquipmentItem;
import com.amh.equipmentmanagement95.model.equipment.EquipmentRequest;
import com.amh.equipmentmanagement95.service.EquipmentService;
import com.amh.equipmentmanagement95.service.common.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "기자재 관리 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/equipment")
public class EquipmentController {

    private final EquipmentService equipmentService;

    @ApiOperation(value = "기자재 정보 등록하기")
    @PostMapping("/new")
    public CommonResult setEquipment(@RequestBody @Valid EquipmentRequest request) {
        equipmentService.setEquipment(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "기자재 정보 가져오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "기자재 시퀀스", required = true)
    })
    @GetMapping("/{id}")
    public SingleResult<EquipmentItem> getEquipment(@PathVariable long id) {
       return ResponseService.getSingleResult(equipmentService.getEquipment(id));
    }

    @ApiOperation(value = "기자재 정보 리스트")
    @GetMapping("/all")
    public ListResult<EquipmentItem> getEquipment() {
        return ResponseService.getListResult(equipmentService.getEquipments(), true);
    }

    @ApiOperation(value = "기자재 정보 수정하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "기자재 시퀀스", required = true)
    })
    @PutMapping("/{id}")
    public CommonResult putEquipment(@PathVariable long id, @RequestBody @Valid EquipmentRequest request) {
        equipmentService.putEquipment(id, request);
        return ResponseService.getSuccessResult();
    }
}
