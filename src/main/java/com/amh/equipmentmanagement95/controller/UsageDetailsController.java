package com.amh.equipmentmanagement95.controller;

import com.amh.equipmentmanagement95.entity.Equipment;
import com.amh.equipmentmanagement95.entity.Member;
import com.amh.equipmentmanagement95.model.common.CommonResult;
import com.amh.equipmentmanagement95.model.common.ListResult;
import com.amh.equipmentmanagement95.model.common.SingleResult;
import com.amh.equipmentmanagement95.model.usageDetails.UsageDetailsItem;
import com.amh.equipmentmanagement95.model.usageDetails.UsageDetailsRequest;
import com.amh.equipmentmanagement95.service.EquipmentService;
import com.amh.equipmentmanagement95.service.MemberService;
import com.amh.equipmentmanagement95.service.UsageDetailsService;
import com.amh.equipmentmanagement95.service.common.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "기자재 사원 이용내역 관리 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/usage-Details")
public class UsageDetailsController {

    private final UsageDetailsService usageDetailsService;
    private final EquipmentService equipmentService;
    private final MemberService memberService;

    @ApiOperation(value = "이용 내역 등록")
    @PostMapping("/new")
    public CommonResult setUsageDetails(@RequestBody @Valid UsageDetailsRequest request) {
        Equipment equipment = equipmentService.getEquipmentData(request.getEquipmentId());
        Member member = memberService.getMemberData(request.getMemberId());

        usageDetailsService.setUsageDetails(equipment, member, request.getDateUsing());
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "이용 내역 한묶음 가져오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "이용내역 시퀀스", required = true)
    })
    @GetMapping("/{id}")
    public SingleResult<UsageDetailsItem> getUsageDetailsOne(@PathVariable long id) {
        return ResponseService.getSingleResult(usageDetailsService.getUsageDetailsOne(id));
    }
    @ApiOperation(value = "이용 내역 전체 가져오기")
    @GetMapping("/all")
    public ListResult<UsageDetailsItem> getUsageDetailsAll() {
        return ResponseService.getListResult(usageDetailsService.getUsageDetailsAll(), true);
    }

}
