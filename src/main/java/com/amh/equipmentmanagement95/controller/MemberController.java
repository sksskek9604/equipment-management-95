package com.amh.equipmentmanagement95.controller;

import com.amh.equipmentmanagement95.model.common.CommonResult;
import com.amh.equipmentmanagement95.model.common.ListResult;
import com.amh.equipmentmanagement95.model.common.SingleResult;
import com.amh.equipmentmanagement95.model.member.MemberInfoAmend;
import com.amh.equipmentmanagement95.model.member.MemberItem;
import com.amh.equipmentmanagement95.model.member.MemberJoinRequest;
import com.amh.equipmentmanagement95.service.MemberService;
import com.amh.equipmentmanagement95.service.common.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "사원 관리 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {

    private final MemberService memberService;

    @ApiOperation(value = "사원 정보 등록하기")
    @PostMapping("/new")
    public CommonResult setMember(@RequestBody @Valid MemberJoinRequest request) {
        memberService.setMember(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "사원 정보 가져오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "사원 시퀀스", required = true)
    })
    @GetMapping("/{id}")
    public SingleResult<MemberItem> getMember (@PathVariable long id) {
        return ResponseService.getSingleResult(memberService.getMember(id));
    }

    @ApiOperation(value = "사원 정보 리스트")
    @GetMapping("/all")
    public ListResult<MemberItem> getMembers () {
        return ResponseService.getListResult(memberService.getMembers(), true);
    }

    @ApiOperation(value = "사원 정보 수정하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "사원 시퀀스", required = true)
    })
    @PutMapping("/{id}")
    public CommonResult putMember(@PathVariable long id, @RequestBody @Valid MemberInfoAmend memberInfoAmend) {
        memberService.putMemberInfo(id, memberInfoAmend);
        return ResponseService.getSuccessResult();
    }
}
