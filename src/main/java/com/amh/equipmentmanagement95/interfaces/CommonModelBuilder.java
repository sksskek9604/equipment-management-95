package com.amh.equipmentmanagement95.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
