package com.amh.equipmentmanagement95.enums.member;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Team {
    MARKETING("마케팅부"),
    SALES("영업부"),
    FINANCIAL("재무부"),
    HUMAN("인사팀");

    private final String name;
}
