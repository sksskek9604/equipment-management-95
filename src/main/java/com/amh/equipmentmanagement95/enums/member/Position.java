package com.amh.equipmentmanagement95.enums.member;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Position {
    INTERN("인턴"),
    STAFF("직원"),
    ASSISTANT_MANAGER("대리"),
    DEPARTMENT_MANAGER("부장"),
    PRESIDENT("사장");

    private final String name;

}
