package com.amh.equipmentmanagement95.repository;

import com.amh.equipmentmanagement95.entity.UsageDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsageDetailsRepository extends JpaRepository<UsageDetails, Long> {
}
