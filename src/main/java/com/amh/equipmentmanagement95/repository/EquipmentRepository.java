package com.amh.equipmentmanagement95.repository;

import com.amh.equipmentmanagement95.entity.Equipment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EquipmentRepository extends JpaRepository<Equipment, Long> {
}
