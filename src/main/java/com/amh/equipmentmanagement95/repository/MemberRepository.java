package com.amh.equipmentmanagement95.repository;

import com.amh.equipmentmanagement95.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
}
