package com.amh.equipmentmanagement95.model.equipment;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class EquipmentRequest {

    @NotNull
    @Length(max = 20)
    @ApiModelProperty(notes = "기자재 이름")
    private String equipmentName;

    @NotNull
    @Length(max = 30)
    @ApiModelProperty(notes = "기자재 관리번호")
    private String managementNum;

    @NotNull
    @Length(max = 20)
    @ApiModelProperty(notes = "기자재 위치")
    private String location;

    @NotNull
    @ApiModelProperty(notes = "기자재 구입일")
    private LocalDate datePurchase;

    @NotNull
    @ApiModelProperty(notes = "기자재 가격")
    private Double price;

    @NotNull
    @ApiModelProperty(notes = "기자재 수량")
    private Integer quantity;

    @NotNull
    @Length(max = 100)
    @ApiModelProperty(notes = "비고란")
    private String note; //비고

}
