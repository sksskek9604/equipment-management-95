package com.amh.equipmentmanagement95.model.equipment;

import com.amh.equipmentmanagement95.entity.Equipment;
import com.amh.equipmentmanagement95.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class EquipmentItem {

    @ApiModelProperty(notes = "기자재 시퀀스")
    private Long id;

    @ApiModelProperty(notes = "기자재 이름") // fullName으로 바꿀것 이름+관리번호
    private String equipmentName;

    @ApiModelProperty(notes = "기자재 관리번호")
    private String managementNum;

    @ApiModelProperty(notes = "기자재 위치")
    private String location;

    @ApiModelProperty(notes = "기자재 구입일")
    private LocalDate datePurchase;

    @ApiModelProperty(notes = "기자재 가격")
    private Double price;

    @ApiModelProperty(notes = "기자재 수량")
    private Integer quantity;

    @ApiModelProperty(notes = "비고란")
    private String note; //비고

    @ApiModelProperty(notes = "등록일")
    private LocalDateTime dateCreate;

    @ApiModelProperty(notes = "수정일")
    private LocalDateTime dateUpdate;

    private EquipmentItem(EquipmentItemBuilder equipmentItemBuilder) {
        this.id = equipmentItemBuilder.id;
        this.equipmentName = equipmentItemBuilder.equipmentName;
        this.managementNum = equipmentItemBuilder.managementNum;
        this.location = equipmentItemBuilder.location;
        this.datePurchase = equipmentItemBuilder.datePurchase;
        this.price = equipmentItemBuilder.price;
        this.quantity = equipmentItemBuilder.quantity;
        this.note = equipmentItemBuilder.note;
        this.dateCreate = equipmentItemBuilder.dateCreate;
        this.dateUpdate = equipmentItemBuilder.dateUpdate;
    }


    public static class EquipmentItemBuilder implements CommonModelBuilder<EquipmentItem> {
        private final Long id;
        private final String equipmentName;
        private final String managementNum;
        private final String location;
        private final LocalDate datePurchase;
        private final Double price;
        private final Integer quantity;
        private final String note; //비고
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public EquipmentItemBuilder(Equipment equipment) {
            this.id = equipment.getId();
            this.equipmentName = equipment.getEquipmentName();
            this.managementNum = equipment.getManagementNum();
            this.location = equipment.getLocation();
            this.datePurchase = equipment.getDatePurchase();
            this.price = equipment.getPrice();
            this.quantity = equipment.getQuantity();
            this.note = equipment.getNote();
            this.dateCreate = equipment.getDateCreate();
            this.dateUpdate = equipment.getDateUpdate();
        }


        @Override
        public EquipmentItem build() {
            return new EquipmentItem(this);
        }
    }

}
