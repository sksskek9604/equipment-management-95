package com.amh.equipmentmanagement95.model.member;

import com.amh.equipmentmanagement95.enums.member.Position;
import com.amh.equipmentmanagement95.enums.member.Team;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberJoinRequest {

    @NotNull
    @ApiModelProperty(notes = "사원 이름")
    private String memberName;

    @NotNull
    @ApiModelProperty(notes = "사원 연락처")
    private String phoneNum;

    @NotNull
    @Enumerated(EnumType.STRING)
    @ApiModelProperty(notes = "사원 직급")
    private Position position;

    @NotNull
    @Enumerated(EnumType.STRING)
    @ApiModelProperty(notes = "사원 소속 팀")
    private Team team;
}
