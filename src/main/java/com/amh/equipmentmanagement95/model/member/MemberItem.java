package com.amh.equipmentmanagement95.model.member;

import com.amh.equipmentmanagement95.entity.Member;
import com.amh.equipmentmanagement95.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberItem {

    @ApiModelProperty(notes = "회원 시퀀스")
    private Long id;

    @ApiModelProperty(notes = "사원 이름")
    private String memberName;

    @ApiModelProperty(notes = "사원 연락처")
    private String phoneNum;

    @ApiModelProperty(notes = "사원 직급")
    private String position;

    @ApiModelProperty(notes = "사원 소속 팀")
    private String team;

    @ApiModelProperty(notes = "사원 입사일")
    private LocalDateTime dateJoin;

    @ApiModelProperty(notes = "사원 정보 수정일")
    private LocalDateTime dateUpdate;

    private MemberItem(MemberItemBuilder memberItemBuilder) {
        this.id = memberItemBuilder.id;
        this.memberName = memberItemBuilder.memberName;
        this.phoneNum = memberItemBuilder.phoneNum;
        this.position = memberItemBuilder.position;
        this.team = memberItemBuilder.team;
        this.dateJoin = memberItemBuilder.dateJoin;
        this.dateUpdate = memberItemBuilder.dateUpdate;

    }
    public static class MemberItemBuilder implements CommonModelBuilder<MemberItem> {

        private final Long id;
        private final String memberName;
        private final String phoneNum;
        private final String position;
        private final String team;
        private final LocalDateTime dateJoin;
        private final LocalDateTime dateUpdate;

        public MemberItemBuilder(Member member) {
            this.id = member.getId();
            this.memberName = member.getMemberName();
            this.phoneNum = member.getPhoneNum();
            this.position = member.getPosition().getName();
            this.team = member.getTeam().getName();
            this.dateJoin = member.getDateJoin();
            this.dateUpdate = member.getDateUpdate();
        }

        @Override
        public MemberItem build() {
            return new MemberItem(this);
        }
    }


}
