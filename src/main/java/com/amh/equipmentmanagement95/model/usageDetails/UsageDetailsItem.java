package com.amh.equipmentmanagement95.model.usageDetails;

import com.amh.equipmentmanagement95.entity.UsageDetails;
import com.amh.equipmentmanagement95.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UsageDetailsItem {

    @ApiModelProperty(notes = "이용 내역 시퀀스")
    private Long usageDetailsId;

    @ApiModelProperty(notes = "이용 날짜")
    private LocalDateTime dateUsing;

    @ApiModelProperty(notes = "기자재 시퀀스")
    private Long equipmentId;

    @ApiModelProperty(notes = "기자재 이름")
    private String equipmentName;

    @ApiModelProperty(notes = "기자재 관리번호")
    private String managementNum;

    @ApiModelProperty(notes = "기자재 위치")
    private String location;

    @ApiModelProperty(notes = "기자재 구입일")
    private LocalDate datePurchase;

    @ApiModelProperty(notes = "기자재 가격")
    private Double price;

    @ApiModelProperty(notes = "기자재 수량")
    private Integer quantity;

    @ApiModelProperty(notes = "기자재 비고")
    private String note;

    @ApiModelProperty(notes = "사원 시퀀스")
    private Long memberId;

    @ApiModelProperty(notes = "사원 이름")
    private String memberName;

    @ApiModelProperty(notes = "사원 연락처")
    private String phoneNum;

    @ApiModelProperty(notes = "사원 직급")
    private String position;

    @ApiModelProperty(notes = "사원 소속팀")
    private String team;

    @ApiModelProperty(notes = "사원 입사일")
    private LocalDateTime dateJoin;

    private UsageDetailsItem(UsageDetailsItemBuilder builder) {
        this.usageDetailsId = builder.usageDetailsId;
        this.dateUsing = builder.dateUsing;
        this.equipmentId = builder.equipmentId;
        this.equipmentName = builder.equipmentName;
        this.managementNum = builder.managementNum;
        this.location = builder.location;
        this.datePurchase = builder.datePurchase;
        this.price = builder.price;
        this.quantity = builder.quantity;
        this.note = builder.note;
        this.memberId = builder.memberId;
        this.memberName = builder.memberName;
        this.phoneNum = builder.phoneNum;
        this.position = builder.position;
        this.team = builder.team;
        this.dateJoin = builder.dateJoin;

    }
    public static class UsageDetailsItemBuilder implements CommonModelBuilder<UsageDetailsItem> {

        private final Long usageDetailsId;
        private final LocalDateTime dateUsing;
        private final Long equipmentId;
        private final String equipmentName;
        private final String managementNum;
        private final String location;
        private final LocalDate datePurchase;
        private final Double price;
        private final Integer quantity;
        private final String note;
        private final Long memberId;
        private final String memberName;
        private final String phoneNum;
        private final String position;
        private final String team;
        private final LocalDateTime dateJoin;

        public UsageDetailsItemBuilder(UsageDetails usageDetails) {
            this.usageDetailsId = usageDetails.getId();
            this.dateUsing = usageDetails.getDateUsing();
            this.equipmentId = usageDetails.getEquipment().getId();
            this.equipmentName = usageDetails.getEquipment().getEquipmentName();
            this.managementNum = usageDetails.getEquipment().getManagementNum();
            this.location = usageDetails.getEquipment().getLocation();
            this.datePurchase = usageDetails.getEquipment().getDatePurchase();
            this.price = usageDetails.getEquipment().getPrice();
            this.quantity = usageDetails.getEquipment().getQuantity();
            this.note = usageDetails.getEquipment().getNote();
            this.memberId = usageDetails.getMember().getId();
            this.memberName = usageDetails.getMember().getMemberName();
            this.phoneNum = usageDetails.getMember().getPhoneNum();
            this.position = usageDetails.getMember().getPosition().getName();
            this.team = usageDetails.getMember().getTeam().getName();
            this.dateJoin = usageDetails.getMember().getDateJoin();
        }

        @Override
        public UsageDetailsItem build() {
            return new UsageDetailsItem(this);
        }
    }

}