package com.amh.equipmentmanagement95.model.usageDetails;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
@Getter
@Setter
public class UsageDetailsRequest {

    @ApiModelProperty(notes = "기자재 시퀀스")
    private Long equipmentId;

    @ApiModelProperty(notes = "사원 시퀀스")
    private Long memberId;

    @ApiModelProperty(notes = "이용 날짜")
    private LocalDateTime dateUsing;
}
