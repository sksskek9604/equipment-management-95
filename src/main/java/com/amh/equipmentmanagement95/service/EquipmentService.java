package com.amh.equipmentmanagement95.service;

import com.amh.equipmentmanagement95.entity.Equipment;
import com.amh.equipmentmanagement95.exception.CMissingDataException;
import com.amh.equipmentmanagement95.model.common.ListResult;
import com.amh.equipmentmanagement95.model.equipment.EquipmentItem;
import com.amh.equipmentmanagement95.model.equipment.EquipmentRequest;
import com.amh.equipmentmanagement95.repository.EquipmentRepository;
import com.amh.equipmentmanagement95.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EquipmentService {


    private final EquipmentRepository equipmentRepository;

    /**
     * 이용 내역에서 사용하기 위한 메서드
     * @param id 기자재 시퀀스
     * @return 기자재
     */
    public Equipment getEquipmentData(long id) {
       Equipment equipment= equipmentRepository.findById(id).orElseThrow(CMissingDataException::new);
        return equipment;
    }

    /**
     * 기자재 등록
     * @param request 기자재 등록 폼
     */
    public void setEquipment(EquipmentRequest request) {
        Equipment equipment = new Equipment.EquipmentBuilder(request).build();
        equipmentRepository.save(equipment);
    }

    /**
     * 기자재 정보 가져오기
     * @param id 기자재 시퀀스
     * @return 기자재 정보
     */
    public EquipmentItem getEquipment(long id) {
        Equipment equipment = equipmentRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new EquipmentItem.EquipmentItemBuilder(equipment).build();
    }

    /**
     * 기자재 정보 리스트
     * @return 기자재 정보 리스트
     */
    public ListResult<EquipmentItem> getEquipments() {
        List<Equipment> equipments = equipmentRepository.findAll();
        List<EquipmentItem> result = new LinkedList<>();

        equipments.forEach(equipment -> {
            EquipmentItem addItem = new EquipmentItem.EquipmentItemBuilder(equipment).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    /**
     * 기자재 정보 수정하기
     * @param id 기자재 시퀀스
     * @param request 기자재 점보 수정 폼
     */
    public void putEquipment(long id, EquipmentRequest request) {
        Equipment equipment = equipmentRepository.findById(id).orElseThrow(CMissingDataException::new);
        equipment.putEquipment(request);
        equipmentRepository.save(equipment);
    }

}
