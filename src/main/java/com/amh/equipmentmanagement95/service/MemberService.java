package com.amh.equipmentmanagement95.service;

import com.amh.equipmentmanagement95.entity.Member;
import com.amh.equipmentmanagement95.exception.CMissingDataException;
import com.amh.equipmentmanagement95.model.common.ListResult;
import com.amh.equipmentmanagement95.model.member.MemberInfoAmend;
import com.amh.equipmentmanagement95.model.member.MemberItem;
import com.amh.equipmentmanagement95.model.member.MemberJoinRequest;
import com.amh.equipmentmanagement95.repository.MemberRepository;
import com.amh.equipmentmanagement95.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {

    private final MemberRepository memberRepository;

    /**
     * 이용 내역에서 사용하기 위한 메서드
     * @param id 사원 시퀀스
     * @return 사원
     */
    public Member getMemberData(long id) {
        Member member= memberRepository.findById(id).orElseThrow(CMissingDataException::new);
        return member;
    }

    /**
     * 사원 등록
     * @param request 사원 등록 폼
     */
    public void setMember(MemberJoinRequest request) {
        Member member = new Member.MemberBuilder(request).build();
        memberRepository.save(member);
    }

    /**
     * 사원 정보 가져오기
     * @param id 사원 시퀀스
     * @return 사원 정보
     */
    public MemberItem getMember(long id) {
        Member member = memberRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new MemberItem.MemberItemBuilder(member).build();
    }

    /**
     * 전체 사원 정보 리스트
     * @return 전체 사원 정보 리스트
     */
    public ListResult<MemberItem> getMembers() {
        List<Member> members = memberRepository.findAll();
        List<MemberItem> result = new LinkedList<>();

        members.forEach(member -> {
            MemberItem addItem = new MemberItem.MemberItemBuilder(member).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    /**
     * 사원 정보 수정
     * @param id 사원 시퀀스
     * @param memberInfoAmend 사원 정보 수정 폼
     */
    public void putMemberInfo(long id, MemberInfoAmend memberInfoAmend) {
        Member member = memberRepository.findById(id).orElseThrow(CMissingDataException::new);
        member.putMember(memberInfoAmend);
        memberRepository.save(member);
    }
}
