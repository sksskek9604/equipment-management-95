package com.amh.equipmentmanagement95.service;

import com.amh.equipmentmanagement95.entity.Equipment;
import com.amh.equipmentmanagement95.entity.Member;
import com.amh.equipmentmanagement95.entity.UsageDetails;
import com.amh.equipmentmanagement95.exception.CMissingDataException;
import com.amh.equipmentmanagement95.model.common.ListResult;
import com.amh.equipmentmanagement95.model.usageDetails.UsageDetailsItem;
import com.amh.equipmentmanagement95.repository.UsageDetailsRepository;
import com.amh.equipmentmanagement95.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UsageDetailsService {

    private final UsageDetailsRepository usageDetailsRepository;

    /**
     * 이용 내역 등록하기
     * @param equipment 기자재
     * @param member 사원
     * @param dateUsing 사용일
     */
    public void setUsageDetails(Equipment equipment, Member member, LocalDateTime dateUsing) {
        UsageDetails usageDetails = new UsageDetails.UsageDetailsBuilder(equipment, member, dateUsing).build();
        usageDetailsRepository.save(usageDetails);
    }

    /**
     * 이용내역 가져오기
     * @param id 이용내역 시퀀스
     * @return 시퀀스 이용내역
     */
    public UsageDetailsItem getUsageDetailsOne(long id) {
        UsageDetails usageDetails = usageDetailsRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new UsageDetailsItem.UsageDetailsItemBuilder(usageDetails).build();
    }

    /**
     * 전체 이용 내역 리스트
     * @return 전체 이용 내역 리스트
     */
    public ListResult<UsageDetailsItem> getUsageDetailsAll() {
        List<UsageDetails> usageDetails = usageDetailsRepository.findAll();
        List<UsageDetailsItem> result = new LinkedList<>();

        usageDetails.forEach(usageDetails1 -> {
            UsageDetailsItem addItem = new UsageDetailsItem.UsageDetailsItemBuilder(usageDetails1).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }
}
