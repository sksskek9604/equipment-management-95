package com.amh.equipmentmanagement95.service.common;

import com.amh.equipmentmanagement95.enums.ResultCode;
import com.amh.equipmentmanagement95.model.common.CommonResult;
import com.amh.equipmentmanagement95.model.common.ListResult;
import com.amh.equipmentmanagement95.model.common.SingleResult;
import org.springframework.stereotype.Service;

@Service
public class ResponseService {

    public static <T> ListResult<T> getListResult(ListResult<T> result, boolean isSuccess) {
        if (isSuccess) setSuccessResult(result);
        else setFailResult(result); //만약 실패했다면
        return result;
    }

    public static <T> SingleResult<T> getSingleResult(T data) {
        SingleResult<T> result = new SingleResult<>();
        result.setData(data);
        setSuccessResult(result);
        return result;
    }

    public static CommonResult getSuccessResult() { //리턴타입이 CommonResult입니다.
        CommonResult result = new CommonResult();
        setSuccessResult(result);
        return result; //리턴타입 위에 result라고 해줬으니
    }

    public static  CommonResult getFailResult(ResultCode resultCode) {
        CommonResult result = new CommonResult();
        result.setIsSuccess(false);
        result.setCode(resultCode.getCode());
        result.setMsg(resultCode.getMsg());
        return result;

    }
    private static void setSuccessResult(CommonResult result) { //(CommonResult모양의 result를 하나 받을 것임)
        result.setIsSuccess(true);
        result.setCode(ResultCode.SUCCESS.getCode()); // (enum만든 것)
        result.setMsg(ResultCode.SUCCESS.getMsg());

    }

    private static void setFailResult(CommonResult result) {
        result.setIsSuccess(false);
        result.setCode(ResultCode.FAILED.getCode());
        result.setMsg(ResultCode.FAILED.getMsg());
    }
}
