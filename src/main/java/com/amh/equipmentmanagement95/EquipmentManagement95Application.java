package com.amh.equipmentmanagement95;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EquipmentManagement95Application {

	public static void main(String[] args) {
		SpringApplication.run(EquipmentManagement95Application.class, args);
	}

}
