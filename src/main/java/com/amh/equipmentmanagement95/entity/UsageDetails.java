package com.amh.equipmentmanagement95.entity;

import com.amh.equipmentmanagement95.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UsageDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "equipmentId", nullable = false)
    private Equipment equipment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @Column(nullable = false)
    private LocalDateTime dateUsing; // 이용날짜 = 등록 날짜

    private UsageDetails(UsageDetailsBuilder builder) {
        this.equipment = builder.equipment;
        this.member = builder.member;
        this.dateUsing = builder.dateUsing;}
    public static class UsageDetailsBuilder implements CommonModelBuilder<UsageDetails> {

        private final Equipment equipment;
        private final Member member;
        private final LocalDateTime dateUsing; // 이용날짜 = 등록 날짜

        public UsageDetailsBuilder(Equipment equipment, Member member, LocalDateTime dateUsing) {
            this.equipment = equipment;
            this.member = member;
            this.dateUsing = dateUsing;
        }
        @Override
        public UsageDetails build() {
            return new UsageDetails(this);
        }
    }
}
