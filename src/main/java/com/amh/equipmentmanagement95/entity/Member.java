package com.amh.equipmentmanagement95.entity;

import com.amh.equipmentmanagement95.enums.member.Position;
import com.amh.equipmentmanagement95.enums.member.Team;
import com.amh.equipmentmanagement95.interfaces.CommonModelBuilder;
import com.amh.equipmentmanagement95.model.member.MemberInfoAmend;
import com.amh.equipmentmanagement95.model.member.MemberJoinRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Member {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String memberName;

    @Column(nullable = false, length = 30)
    private String phoneNum;

    @Column(nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    private Position position; // 직급

    @Column(nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    private Team team; // 소속팀

    @Column(nullable = false)
    private LocalDateTime dateJoin; // 입사일

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putMember(MemberInfoAmend memberInfoAmend) {
        this.memberName = memberInfoAmend.getMemberName();
        this.phoneNum = memberInfoAmend.getPhoneNum();
        this.position = memberInfoAmend.getPosition();
        this.team = memberInfoAmend.getTeam();
    }
    private Member(MemberBuilder memberBuilder) {
        this.memberName = memberBuilder.memberName;
        this.phoneNum = memberBuilder.phoneNum;
        this.position = memberBuilder.position;
        this.team = memberBuilder.team;
        this.dateJoin = memberBuilder.dateJoin;
        this.dateUpdate = memberBuilder.dateUpdate;

    }
    public static class MemberBuilder implements CommonModelBuilder<Member> {
        private final String memberName;
        private final String phoneNum;
        private final Position position;
        private final Team team;
        private final LocalDateTime dateJoin;
        private final LocalDateTime dateUpdate;

        public MemberBuilder (MemberJoinRequest request) {
            this.memberName = request.getMemberName();
            this.phoneNum = request.getPhoneNum();
            this.position = request.getPosition();
            this.team = request.getTeam();
            this.dateJoin = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public Member build() {
            return new Member(this);
        }
    }
}
