package com.amh.equipmentmanagement95.entity;

import com.amh.equipmentmanagement95.interfaces.CommonModelBuilder;
import com.amh.equipmentmanagement95.model.equipment.EquipmentRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Equipment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String equipmentName;

    @Column(nullable = false, length = 30)
    private String managementNum;

    @Column(nullable = false, length = 20)
    private String location; // 위치

    @Column(nullable = false)
    private LocalDate datePurchase; // 구입일

    @Column(nullable = false)
    private Double price; // 가격

    @Column(nullable = false)
    private Integer quantity; // 수량

    @Column(nullable = false, length = 100)
    private String note; //비고

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putEquipment (EquipmentRequest request) {
        this.equipmentName = request.getEquipmentName();
        this.managementNum = request.getManagementNum();
        this.location = request.getLocation();
        this.datePurchase = request.getDatePurchase();
        this.price = request.getPrice();
        this.quantity = request.getQuantity();
        this.note = request.getNote();
        this.dateUpdate = LocalDateTime.now();

    }

    private Equipment(EquipmentBuilder builder) {
        this.equipmentName = builder.equipmentName;
        this.managementNum = builder.managementNum;
        this.location = builder.location;
        this.datePurchase = builder.datePurchase;
        this.price = builder.price;
        this.quantity = builder.quantity;
        this.note = builder.note;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;

    }
    public static class EquipmentBuilder implements CommonModelBuilder<Equipment> {

        private final String equipmentName;
        private final String managementNum;
        private final String location; // 위치
        private final LocalDate datePurchase; // 구입일
        private final Double price; // 가격
        private final Integer quantity; // 수량
        private final String note; //비고
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public EquipmentBuilder(EquipmentRequest request) {
            this.equipmentName = request.getEquipmentName();
            this.managementNum = request.getManagementNum();
            this.location = request.getLocation();
            this.datePurchase = request.getDatePurchase();
            this.price = request.getPrice();
            this.quantity = request.getQuantity();
            this.note = request.getNote();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();

        }

        @Override
        public Equipment build() {
            return new Equipment(this);
        }
    }


}
